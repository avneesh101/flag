    <?php
    App::uses('CakeEmail', 'Network/Email');

    class MessagesController extends AppController {
         public $components = array('Flash', 'Auth', 'Session');

    Public function beforeFilter() {
        $this->Auth->allow('login');
    }

    public function isAuthorized() {
        // Here is where we should verify the role and give access based on role

            return true;
        }


    public function ajax_chatroom(){
         $this->autoRender = $this->layout = FALSE;
         
           //debug($this->request->query['msg']);die;
         if( $this->request->is('ajax') ) {
            //debug($this->request->query['msg']);die;
            $result= $this->request->query('msg');
            //debug($result);die;
            $u_id = AuthComponent:: user('id');
            //debug($u_id);die;
            $data = array('Message' => array(
                            'user_id' => $u_id,
                            'message' => $result,
                            
                    ));
            //debug($data);die;

            $this->loadModel('Message');
            $this->Message->save($data);
        }
    }

    public function chatroom(){
      $this->layout ='chatroom';
     // //debug($this->request->query['msg']);die;
     // if (!isset($this->request->data->query['msg'])) { 
     //    $rslt= $this->request->query('msg');
     //    //debug($rslt)  ;die;
     //    $this->loadModel('Message');
     //    $this->Message->save($rslt);
     //    $this->set("result", $rslt);


                        //code to get data and process it here

    //}
    }

}
?>
