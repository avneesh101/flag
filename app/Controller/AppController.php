<?php

App::uses('Controller', 'Controller');

class AppController extends Controller {
    public $components = array(
    'Session',
    'Auth' => array(
        
        'loginRedirect' => array('controller' => 'users', 'action' => 'login'),
        'logoutRedirect' => array('controller' => 'users', 'action' => 'logout'),
        'authError' => 'You must be logged in to view this page.',
        'loginError' => 'Invalid Username or Password entered, please try again.'
 
    ),
    );
 
public function beforeFilter() {
    $this->Auth->allow('login');
   $user = AuthComponent:: user('id');
    $this->loadModel('Image');
     $cnd = array('Image.user_id'=> $user);
    $img = $this->Image->find('all' , array('order'=>array('Image.id'=>'desc'),'limit' => 1), array('conditions'=>$cnd));
    $this->set('imgg',$img);

    
}
  function beforeRender() {

  }
public function isAuthorized() {
   
     
    return true;
}
}
?>
