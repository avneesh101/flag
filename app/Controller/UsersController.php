    <?php
    App::uses('CakeEmail', 'Network/Email');

    class UsersController extends AppController {
        public $helpers = array('Html', 'Form', 'Flash');
        public $components = array('RequestHandler','Flash',
            'session',
            'Auth' => array(
             'authenticate' => array(
                'Form' => array(
                 'fields' => array('username' => 'mobile', 'password' => 'password')
                 )
                )
             ),
            );

    public $paginate = array(
            'limit' => 25,
            'conditions' => array('status' => '1'),
            'order' => array('User.username' => 'asc' ) 
            );
    public function initialize()
        {
            parent::initialize();
            $this->loadComponent('Auth', [
                'loginAction' => [
                'controller' => 'Users',
                'action' => 'login',
                'plugin' => 'Users'
                ], 
                ]);
        }


    public function beforeFilter() {
            parent::beforeFilter();
            $this->Auth->allow('login','add','logout'); 
        }
        public function isAuthorized() {
        // Here is where we should verify the role and give access based on role

            return true;
        }


    public function login() {
         $this->layout='login';
         if ($this->request->is('post')) {

            if ($this->Auth->login() ){

             $this->Flash->success('You are logged in');
             return $this->redirect($this->Auth->redirectUrl('chatroom'));


         } 
         else {
            $this->Flash->success('Invailed username & password!');
        } 
    }
}
    public function logout() {



    $this->Session->destroy('User');
    
    $this->Auth->logout();
    
    $this->redirect("login");


}

    public function add() {
    $this->layout='login';
    
    if ($this->request->is('post')) {

        $this->User->create();
        if ($this->User->save($this->request->data)) {
            $this->redirect(array('action' => '/login')); 
            $this->Flash->success('The user has been created');
            
        } else {
            $this->Session->setFlash(__('The user could not be created. Please, try again.'));
        }   
    }
}
    public function users_header(){

}

    public function ajax_chatroom(){
    $this->autoRender = $this->layout = FALSE;
    $response = array();
    
    if( $this->request->is('ajax') ) {

        $result= $this->request->query('msg');
        
        $u_id = AuthComponent:: user('id');
        
        $data = array('Message' => array(
            'user_id' => $u_id,
            'message' => $result,
            
            ));
        

        $this->loadModel('Message');
        $this->Message->save($data);
        
        $id = $this->Message->getLastInsertId();
        //debug($id);die;
        $cnd  = array('Message.user_id'=> $u_id , 'Message.id' => $id);
        //debug($cnd);die;
        $var = $this->Message->find('all', array('conditions'=>$cnd));
        $response['success'] = true;
        $response['result'] = $var;
        $this->response->body(json_encode($response));
        
        return $this->response;


        
    }
}
    public function chatroom(){
     $this->layout = 'chatroom';
     $u_id = AuthComponent:: user('id');
     $this->loadModel('Message');
     $old = $this->Message->find('all');
             //$dd = $old['Message']['id'];

             //debug($old);die;
     $this->set('old_data' , $old);

}




    public function myprofile(){
    $this->layout = 'chatroom';
    $this->loadModel('Message');
    $user = AuthComponent:: user('id');
            //debug($user);die;
    $cond  = array('User.id'=> $user);
    $chk = $this->User->find('first', array('conditions'=>$cond));
    $this->set('ck_prfl',$chk);
            //debug($chk);die;
    $this->loadModel('Image');
    $cnd = array('Image.user_id'=> $user);
            //$id = $this->Image->getLastInsertId();
           // debug($id);die;
    $img = $this->Image->find('all' , array('order'=>array('Image.id'=>'desc'),'limit' => 1), array('conditions'=>$cnd));
            //debug($img);die;
    $this->set('imgg',$img);
} 

    public function fileupload(){
      
     $this->autoRender = $this->layout = FALSE;
     if ($this->request->is('post')) {
               //debug($this->request->data);die;
        $filename = $this->request->data['Message']['filename']['name'];
        $filename = str_replace(' ', '_', $filename);
        $this->request->data['Message']['filename']['name'] = $filename;
        $user_id = AuthComponent:: user('id');
        
        $validate = array(
            'filename' => array(
                'extension' => array(
                    'rule' => 'checkFIle',
                    'message' => 'Upload a image and should not exceed 2MB'
                    ),
                
                ),
            );
        $this->loadModel('Message');
        $this->Message->set($this->request->data);
                //debug($this->Media->set($this->request->data));die;
        $this->Message->validate = $validate;
                //debug($validate);die;
        if ($this->Message->validates($this->request->data)) { 
                    //debug($id);die;
            move_uploaded_file($this->request->data['Message']['filename']['tmp_name'], WWW_ROOT . DS . 'chatlog' . DS . $filename);
                //debug(move_uploaded_file($this->request->data['Media']['filename'], WWW_ROOT . DS . 'chatlog' . DS . $filename));die;
            $data = array('Message' => array(
                'user_id' => $user_id,
                
                'filename' => $filename,
                'file_path' => '/' . 'chatlog' . '/' . $filename
                ));
                    //debug($data);die;
            $this->loadModel('Message');
            if ($this->Message->save($data, false)) {


                $this->redirect('chatroom');


            }


        } 
        
    }


    }

    public function imageupload(){

     $this->autoRender = $this->layout = FALSE;
     if ($this->request->is('post')) {
               //debug($this->request->data);die;
        $profile_pic = $this->request->data['Image']['profile_pic']['name'];
                //debug($profile_pic);die;
        $profile_pic = str_replace(' ', '_', $profile_pic);
        $this->request->data['Image']['profile_pic']['name'] = $profile_pic;
                 //debug($profile_pic);die;
        $u_id = AuthComponent:: user('id');
        $validate = array(
            'profile_pic' => array(
                'extension' => array(
                    'rule' => 'checkImg',
                    'message' => 'Upload a image and should not exceed 2MB'
                    ),
                
                ),
            );
        $this->loadModel('Image');
        $this->Image->set($this->request->data);
                //debug($this->Image->set($this->request->data));die;
        $this->Image->validate = $validate;
                //debug($this->Image->validates($this->request->data));die;
        if ($this->Image->validates($this->request->data)) { 
                //debug($this->request->data);die;
            move_uploaded_file($this->request->data['Image']['profile_pic']['tmp_name'], WWW_ROOT . DS . 'pro_pic' . DS . $profile_pic);
                //debug(move_uploaded_file($this->request->data['Image']['profile_pic']['tmp_name'], WWW_ROOT . DS . 'pro_pic' . DS . $profile_pic));die;
            $data = array('Image' => array(
                'user_id' => $u_id,
                'profile_pic' => $profile_pic,
                'img_path' => '/' . 'pro_pic' . '/' . $profile_pic
                ));
            
            $this->loadModel('Image');
            if ($this->Image->save($data, false)) {

                         // $id = $this->Image->getLastInsertId();
                         // debug($id);die;
                $this->redirect('myprofile');
                



            }


        } 
        
    }


    }






}
?>
