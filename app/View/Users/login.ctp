
                <!-- BEGIN LOGIN FORM -->
                <?php echo $this->Session->flash('auth'); ?>
                    <?php echo $this->Form->create('User');?>
                        <form class="login-form" action="login" method="post">
                            <h3 class="form-title font-green">Sign In</h3>
                            
                           <div class="form-group">
                                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                                <label class="control-label visible-ie8 visible-ie9">mobile no.</label>
                                <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="mobile no." name="User[mobile]" /> </div>
                            <div class="form-group">
                                <label class="control-label visible-ie8 visible-ie9">Password</label>
                                <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="User[password]" /> </div>
                            <div class="form-actions">
                                <button type="submit" class="btn green uppercase">Login</button>
                                 </div>
                            <div class="create-account"style="margin-top:50px">
                                  <p>
                                    <a href="add" id="register-btn" class="uppercase">Create an account</a>
                                </p>
                           
                        </form>
                        <!-- END LOGIN FORM -->
                        
                        <!-- BEGIN REGISTRATION FORM -->
                        
                   



