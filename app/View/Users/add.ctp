                <!-- BEGIN REGISTRATION FORM -->
                 <?php echo $this->Session->flash('auth'); ?>
                <?php echo $this->Form->create('User'); ?>
                <form  action="login" method="post">
                    <h3 class="font-green">Sign Up</h3>
                    <p class="hint"> Enter your personal details below: </p>
                    <div class="form-group">
                        <label class="control-label visible-ie8 visible-ie9"> Name</label>
                        <input class="form-control placeholder-no-fix" type="text" placeholder="Full Name" name="name" required /> 
                        <label class="control-label visible-ie8 visible-ie9">mobile no.</label>
                        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="mobile no." name="mobile" required/> 
                   
                        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                        <label class="control-label visible-ie8 visible-ie9">Email</label>
                        <input class="form-control placeholder-no-fix" type="text" placeholder="Email" name="email"  /> 
                        
                    
                        <label class="control-label visible-ie8 visible-ie9">Password</label>
                        <input class="form-control placeholder-no-fix" type="password" autocomplete="off" id="register_password" placeholder="Password" name="password" required/>
                    

                               <!--  <span class="input-group-addon btn default btn-file"><h5 style="float:left">profile_pic:</h5>
                                   
                                    
                                    <?php echo $this->Form->User('profile_pic', array('type' => 'file', 'class' => 'validate[required] text-input', 'id' => 'file')); ?>
                                </span> -->
                    <div class="form-group margin-top-20 margin-bottom-20">
                        <label class="check">
                            <input type="checkbox" name="tnc" /> I agree to the
                            <a href="javascript:;"> Terms of Service </a> &
                            <a href="javascript:;"> Privacy Policy </a>
                        </label>
                        <div id="register_tnc_error"> </div>
                    </div>
                    <div class="form-actions">
                        
                        <button type="submit" id="register-submit-btn" class="btn btn-success uppercase pull-right">Submit</button>
                    </div>
                </div>
                </form>
                <!-- END REGISTRATION FORM -->
