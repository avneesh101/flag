            <?php

            App::uses('AuthComponent', 'Controller/Component');

            class User extends AppModel {

            public $hasMany = array(
            'Message' => array(
            'className' => 'Message',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
           

            'Image' => array(
            'className' => 'Image',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    //         public $hasMany = array(
    //         'Media' => array(
    //         'className' => 'Media',
    //         'foreignKey' => 'user_id',
    //         'conditions' => '',
    //         'fields' => '',
    //         'order' => ''
    //     )
    // );
                public $avatarUploadDir = 'img/avatars';
                public $validate = array(
                    'name' => array(
                        'required' => array(
                            'rule' => array('notBlank'),
                            'message' => 'A name is required',
                            'allowEmpty' => false
                        ),
                        'between' => array(
                            'rule' => array('between', 1, 10),
                            'required' => true,
                            'message' => 'names must be between 1 to 10 characters'
                        ),
                        'alphaNumericDashUnderscore' => array(
                            'rule' => array('alphaNumericDashUnderscore'),
                            'message' => 'country can only be letters, numbers and underscores'
                        ),
                       
                    ),
                    
                    'mobile' => array(
                        'required' => array(
                            'rule' => array('notBlank'),
                            'message' => 'A mobile_no is required',
                            'allowEmpty' => false
                        ),
                        'between' => array(
                            'rule' => array('between', 1, 15),
                            'required' => true,
                            'message' => 'mobile_no must be between 1 to 10 characters'
                        ),
                        
                    ),
                    'password' => array(
                        'required' => array(
                            'rule' => array('notBlank'),
                            'message' => 'A password is required'
                        ),
                        'min_length' => array(
                            'rule' => array('minLength', '6'),
                            'message' => 'Password must have a mimimum of 6 characters'
                        )
                    )
                    
                );
              public function alphaNumericDashUnderscore($check) {
                    // $data array is passed using the form field name as the key
                    // have to extract the value to make the function generic
                    $value = array_values($check);
                    $value = $value[0];
                    //dd $value;

                    return preg_match('/^[a-zA-Z0-9_ \-]*$/', $value);
                }

             

                 public function beforeSave($options = array()) {
                    // hash our password
                    if (isset($this->data[$this->alias]['password'])) {
                        $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
                    }

                    // if we get a new password, hash it
                    // if (isset($this->data[$this->alias]['password_update']) {
                    //     $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password_update']);
                    // }
                    // fallback to our parent
                   $this->data['User']['resetkey'] = Security::hash(mt_rand(),'md5',true);
            return true;
  
                }
            }
