-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 24, 2016 at 06:56 AM
-- Server version: 5.5.36
-- PHP Version: 5.4.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `kms`
--

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE IF NOT EXISTS `files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `perm_id` int(5) NOT NULL DEFAULT '1',
  `title` varchar(50) NOT NULL,
  `meta` varchar(400) NOT NULL,
  `file_name` varchar(200) NOT NULL,
  `file_path` varchar(400) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`id`, `perm_id`, `title`, `meta`, `file_name`, `file_path`, `created`, `modified`) VALUES
(1, 1, 'CakePHP Application Development', 'Cake is a rapid development framework for PHP that uses well-known design patterns and provides a structured framework that enables PHP users at all levels to rapidly develop robust web applications, without any loss of flexibility. This book builds on your knowledge as a PHP developer to provide a fast-paced, step-bystep\ntutorial in building CakePHP applications.', 'sample_chapter.pdf', '/PDF_Files/sample_chapter.pdf', '2016-08-22 16:04:18', '2016-08-22 16:04:18'),
(2, 1, 'Historical Importance of Haryana', 'Haryana came into existence as a new state of the Union of India on November 1, 1966, when it was carved out of Punjab. It is located in the north-western part of India and bordered by Uttar Pradesh and Delhi in the east, Punjab in the north-west, Himachal Pradesh in the north and Rajasthan in the south-west. The ancient land of Haryana, which has been traditionally known as a seat of renowned sag', 'haryana.pdf', '/PDF_Files/haryana.pdf', '2016-08-22 17:20:28', '2016-08-22 17:20:28'),
(3, 1, 'title', 'meta', 'websitecontent.doc', '/DOC_Files/websitecontent.doc', '2016-08-22 17:40:27', '2016-08-22 17:40:27'),
(4, 1, 'title', 'meta', 'RESUME_(Jatinder_Singh).docx', '/DOC_Files/RESUME_(Jatinder_Singh).docx', '2016-08-23 08:56:14', '2016-08-23 08:56:14');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
